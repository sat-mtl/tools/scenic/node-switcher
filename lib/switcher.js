'use strict'

/**
 * Switcher Node Addon Loader
 * @module node-scenic/lib/switcher
 */

try {
  module.exports = require('../build/Release/switcher')
} catch (e) {
  console.error(e)
  try {
    module.exports = require('../build/Debug/switcher')
  } catch (e) {
    console.error('Error loading switcher addon.')
  }
}
