/**
 * @constant JSDOC_COMMENT_START_REGEX
 * @description Match starting strings of a jsdoc comment
 */
const JSDOC_COMMENT_START_REGEX = /^\/\*\*/

/**
 * @constant JSDOC_COMMENT_MIDDLE_REGEX
 * @description Match middle strings of a jsdoc comment
 */
const JSDOC_COMMENT_MIDDLE_REGEX = /^\s\*\s?/

/**
 * @constant JSDOC_COMMENT_END_REGEX
 * @description Match ending strings of a jsdoc comment
 */
const JSDOC_COMMENT_END_REGEX = /^\s\*\//

/**
 * @constant CPP_REGEX
 * @description Match cpp file names
 */
const CPP_REGEX = /.*\.cpp$/

/**
 * @function clean_source
 * @description Clean a source code file by keeping only jsdoc comments
 * @param {string} [source=''] The source code to clean 
 */
const clean_source = function (source = '') {
  let started = false
  let cleaned = []

  for (const line of source.split('\n')) {
    if (!started && line.match(JSDOC_COMMENT_START_REGEX)) {
      started = true
      cleaned.push(line)
    } else if (started && line.match(JSDOC_COMMENT_END_REGEX)) {
      started = false
      cleaned.push(line, '')
    } else if (started && line.match(JSDOC_COMMENT_MIDDLE_REGEX)) {
      cleaned.push(line)
    }
  }

  return cleaned.join('\n')
}

/**
 * JSDOC plugin for C++ NodeJS addon
 * @module jsdoc/plugins/CppAddons
 * @see {@link https://jsdoc.app/about-plugins.html jsdoc plugins}
 */
exports.handlers = {
  beforeParse: function (e) {
    if (e.filename.match(CPP_REGEX)) {
      e.source = clean_source(e.source)
    }
  }
}
