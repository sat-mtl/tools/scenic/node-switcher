#!/usr/bin/env node

;(function () {
  const repl = require('repl')

  console.log('Hello, you can start Switcher scripting by using `new Switcher(\'debug\', console.log)`')
  
  const r = repl.start('node::switcher> ')

  r.context.Switcher = require('../lib/switcher').Switcher
})()
