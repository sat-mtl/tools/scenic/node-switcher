import os
import re
import datetime

from json import loads
from subprocess import check_output, call

def get_git_config(property, default_value):
  config_property = default_value
  output = check_output('git config --list', shell=True)
  config_full = output.decode('utf-8').strip().split('\n')

  for config in config_full:
    prop = config.split('=')

    if len(prop) < 2:
      break
    if prop[0] == property:
      config_property = prop[1]

  return config_property

def get_message_from(commit):
  elements = list(filter(None, commit.split('\n    ')))

  if len(elements) < 1:
    return None

  del elements[0] # Delete author and date entry

  if '/release' in elements[0] or len(elements) < 1:
    return None
  elif len(elements) == 1:
    return elements[0].strip()
  else:
    return elements[1].strip()

def strip_commit_prefix_of(message):
  msg = message.lower()

  if ('feature:' in msg or 'feat:' in msg or 'fix:' in msg or
      'tooling:' in msg or 'refactoring' in msg or 'documentation:' in msg):
    return message[message.find(':')+1:].lstrip()
  else:
    return message.strip()

def get_last_git_tag_date():
  out = check_output('git describe --tags `git rev-list --tags --max-count=1`', shell=True)
  latest_tag = out.decode('utf-8').strip()

  out = check_output(f'git log -1 --format=%ai {latest_tag}', shell=True)
  tag_date = out.decode('utf-8').strip()

  return tag_date

def get_all_commit_message_until(tag_date=None):
  if tag_date is not None:
    output = check_output(f'git log --first-parent --since="{tag_date}"', shell=True)
  else:
    output = check_output('git log --first-parent', shell=True)

  log = re.split(r'commit [a-z0-9]+', output.strip().decode('utf-8'))
  all_messages = []

  for commit in log:
    message = get_message_from(commit)

    if message is not None:
      clean_message = strip_commit_prefix_of(message)
      all_messages.append(clean_message)

  return all_messages

def get_package_infos(default_version='0.0.0', default_name='Package'):
  version = default_version
  name = default_name

  with open('package.json', 'r') as package_file:
    package_json = loads(package_file.read())
    version = package_json['version']
    name = package_json['name']

  return version, name

def craft_release_header_from(lib_name, lib_version):
  current_date =  datetime.date.today()
  horizontal_rule = '-' * 34

  return f'\n{lib_name} {lib_version} ({current_date})\n{horizontal_rule}\n\n'

if __name__ == "__main__":
  print('Generating release notes')

  orig_file_name = 'CHANGELOG.md'
  new_file_name = 'CHANGELOG.md.new'
  authors_file_name = 'AUTHORS.md'

  version, lib_name = get_package_infos()
  last_tag_date = get_last_git_tag_date()

  commits = get_all_commit_message_until(last_tag_date)

  with open(new_file_name, 'w') as new_file:
    with open(orig_file_name, 'r') as old_file:
      for i, line in enumerate(old_file.readlines()):
        print(line)

        if i != 2:
          new_file.write(line)
          continue

        new_file.write(craft_release_header_from(lib_name, version))

        for commit in commits:
          new_file.write('* {}\n'.format(commit))

    new_file.write('\n\n')

  call([get_git_config('core.editor', 'vim'), new_file_name])
  os.rename(new_file_name, orig_file_name)
